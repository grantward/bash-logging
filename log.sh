#!/bin/bash

# filename, line_limit
trim_file () {
  if [ $2 -le 0 ]
  then
    return 0
  fi
  line_count=$(wc -l $1 | grep -oE "[0-9]+")
  if [ $line_count -ge $2 ]
  then
    tail -n $(expr $2 / 2) $1 > "$1.tmp"
    mv "$1.tmp" $1
  fi
}

# level, message
log () {
  cur_time=$(date '+%Y%m%d-%H%M')
  printf "%s: %s: %s\n" "${1^^}" "$cur_time" "$2"
}

# filename, level, message, line limit
log_file () {
  log "$2" "$3" >> $1
  trim_file $1 $4
}

# See above, also prints
log_file_echo () {
  msg=$(log "$2" "$3")
  echo "$msg" >> $1
  echo "$msg"
  trim_file $1 $4
}

if [ $# -eq 4 ]
then
  # Full log with line limit
  log_file_echo "$1" "$2" "$3" "$4"
elif [ $# -eq 3 ]
then
  # Log without line limit
  log_file_echo "$1" "$2" "$3" 0
else
  # Incorrect usage
  printf "Usage: %s <filename> <log level> <log message> [line limit]\n" "$0"
fi
